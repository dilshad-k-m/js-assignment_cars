// defining the function that returns only Audi and BMW cars
module.exports = function sol(x) {
    const y=[];
    //iterating through the car array for the car information of Audi and BMW
    for(let idx=0;idx<x.length;idx++) {
        if(x[idx].car_make === 'BMW' || x[idx].car_make === 'Audi'){
            y.push(x[idx]);
        }
    }
    return y;
}