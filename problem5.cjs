// creating a function that returns old cars
module.exports = function sol(x) {
    const y = [];
    //iterating through the array and add the years before 2000
    for (let idx=0; idx<x.length;idx++) {
        if (x[idx]<2000) {
            y.push(x[idx]);
        }
    }
    return [y, y.length];
};