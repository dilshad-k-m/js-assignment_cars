//Exporting the module with function 'sol' that returns the car object with id 33 where 'x' is the parameter for passing the inventory array
module.exports = function sol(x,id) {
    //Return empty array if no parameters are passed or if no search id is passed or if an empty array is passed as inventory array
    if (arguments.length ===0 || id == null || x.length === 0 || typeof id !== "number" || !Array.isArray(x)){
        return [];
    }
    //Iterating through the array for getting required car details 
    for (let idx=0; idx<x.length; idx++) {
        if (x[idx].id === id) {
            return x[idx];
        }
    }
    // If there is no car with the provided id
    return [];
};