// Declaring a function that returns all the years of car
module.exports = function sol(x) {
    const y = [];
    // iterating through cars array
    for (let idx=0; idx < x.length; idx++) {
        y.push(x[idx].car_year);
    }
    return y;
};